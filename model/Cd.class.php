<?php

class Cd{
    private $titulo;
    private $descricao;
    private $disponivel;
    private $ano;
    private $musica;
    private $artista;
    private $categoria;
    private $valor;
    private $capa;
    function getTitulo() {
        return $this->titulo;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function getDisponivel() {
        return $this->disponivel;
    }

    function getAno() {
        return $this->ano;
    }

    function getMusica() {
        return $this->musica;
    }

    function getArtista() {
        return $this->artista;
    }

    function getCategoria() {
        return $this->categoria;
    }

    function getValor() {
        return $this->valor;
    }

    function getCapa() {
        return $this->capa;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setDisponivel($disponivel) {
        $this->disponivel = $disponivel;
    }

    function setAno($ano) {
        $this->ano = $ano;
    }

    function setMusica($musica) {
        $this->musica = $musica;
    }

    function setArtista($artista) {
        $this->artista = $artista;
    }

    function setCategoria($categoria) {
        $this->categoria = $categoria;
    }

    function setValor($valor) {
        $this->valor = $valor;
    }

    function setCapa($capa) {
        $this->capa = $capa;
    }


    
    
}