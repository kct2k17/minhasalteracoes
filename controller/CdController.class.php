<?php
 require_once './dao/CdDAO.class.php';
 switch ($acao) {

    case 'salvar1':

      if (isset($_POST['nome'])) {
              
        $titulo = $_POST['titulo'];
        $descricao = $_POST['descricao'];
        $ano = $_POST['ano'];
        $valor = $_POST['valor'];
        $musica = $_POST['musica'];
        $artista = $_POST['artista'];
        $categoria = $_POST['categoria'];
        $disponivel = $_POST['disponivel'];
        

        $oCd = new Cd();
        $oCd->setNome($titulo);
        $oCd->setDescricao($descricao);
        $oCd->setValor($valor);    
        $oCd->setMusica($musica);    
        $oCd->setArtista($artista);    
        $oCd->setCategoria($categoria);    
        $oCd->setDisponivel($disponivel);    
        if (DAOFactory::getCDDAO()->salvar($oCd)) {
          $cod = 100;
        } else {
          $cod = 102;
        }
        header('Location: index.php?area=cd&acao=cad1&cod='.$cod);
      }


      break;

    case 'excluir1':

      if ($_GET['id']) {                
        
        $idExcluir = $_GET['id'];
        $resultado = DAOFactory::getCdDAO()->excluir($idExcluir);
        if ($resultado) {
          echo "excluído com sucesso";
        } else {
          echo "não foi possível excluir";
        }
      }

      break;

    case 'listar1':
            
      $lista = DAOFactory::getCdDAO()->listar();
      
      include './view/lista_cd.php';
      break;

    case 'cad1':

      include './view/cad_cd.php';

      break;
  }