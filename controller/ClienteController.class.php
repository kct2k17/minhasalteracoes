<?php
  
  require_once './dao/ClienteDAO.class.php';
  //Controle Pessoa
  
  switch ($acao) {

    case 'salvar':

      if (isset($_POST['nome'])) {
              
        $nome = $_POST['nome'];
        $email = $_POST['email'];
        $telefone = $_POST['telefone'];

        $oCliente = new Cliente();
        $oCliente->setNome($nome);
        $oCliente->setTelefone($telefone);
        $oCliente->setEmail($email);    
        
        if (DAOFactory::getClienteDAO()->salvar($oCliente)) {
          $cod = 100;
        } else {
          $cod = 102;
        }
        header('Location: index.php?area=cliente&acao=cad&cod='.$cod);
      }


      break;

    case 'excluir':

      if ($_GET['id']) {                
        
        $idExcluir = $_GET['id'];
        $resultado = DAOFactory::getClienteDAO()->excluir($idExcluir);
        if ($resultado) {
          echo "excluído com sucesso";
        } else {
          echo "não foi possível excluir";
        }
      }

      break;

    case 'listar':
            
      $lista = DAOFactory::getClienteDAO()->listar();
      
      include './view/lista_cliente.php';
      break;

    case 'cad':

      include './view/cad_cliente.php';

      break;
  }