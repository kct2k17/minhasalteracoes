<?php

require './dao/DAOFactory.class.php';
require './dao/GenericDAO.class.php';
class CdDAO extends GenericDAO {

    public function salvar1(Cd $oCd) {

      $this->conexao->beginTransaction();
      try {

        if (!$oCd->getId()) { //Inserir um novo registro
          
          $query = $this->conexao->prepare("INSERT INTO cd (titulo, descricao, disponivel, ano, musica, artista, categoria, valor) "
            . " values (:titulo, :descricao, :disponivel, :ano, :musica, :artista, :categoria, :valor)");
          
        } else {//Alterar um registro existente

          $query = $this->conexao->prepare("UPDATE cd SET titulo = :titulo, "
            . "descricao = :descricao, disponivel = :disponivel, ano = :ano, musica = :musica, artista = :artista, categoria = categoria, valor = :valor WHERE id = :id");
          
          $query->bindParam(':id', $oCd->getId());
          
        }
        
        $query->setNome($titulo);
        $query->setDescricao($descricao);
        $query->setDisponivel($disponivel);
        $query->setAno($ano);
        $query->setValor($valor);    
        $query->setMusica($musica);    
        $query->setArtista($artista);    
        $query->setCategoria($categoria);    
            
        $query->execute();        
        $this->conexao->commit();
        
        return TRUE;
        
      } catch (PDOException $ex) {

        $this->conexao->rollback();
        return FALSE;
        
      }
}

      public function excluir($id) {

      $this->conexao->beginTransaction();
      try {

        if ($id > 0) { //Excluir cd
          $query = $this->conexao->prepare("DELETE FROM cd WHERE id = :id");
          $query->bindParam(':id', $id);
          $query->execute();
        }

        $this->conexao->commit();
        return TRUE;
      } catch (PDOException $ex) {

        $this->conexao->rollback();
        return FALSE;
      }
    }

    public function listar() {

      $this->conexao->beginTransaction();
      try {

        $query = $this->conexao->prepare("SELECT id, titulo,"
          . " descricao, disponivel, ano, musica, artista, categoria, valor  FROM cd");
        $query->execute();
        
        return $query->fetchALL(PDO::FETCH_CLASS, 'Cd');
        
        
      } catch (PDOException $ex) {

        $this->conexao->rollback();
        return NULL;
      }
    }

  }