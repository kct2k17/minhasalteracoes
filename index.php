<?php require './autoloader.php'; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Loja Web</title>
    </head>
    <body>
        <div name="cadastro pessoa">
            <nav>
                <a href="?area=cliente&acao=cad">Cadastrar pessoa</a>
                <a href="?area=cliente&acao=listar">Listar pessoas</a>
            </nav>
        </div<br>
        <div name="cadastro cd">
            <nav>
                <a href="?area=cd&acao=cad1">Cadastrar cd</a>
                <a href="?area=cd&acao=listar1">Listar cd</a>
            </nav>
        </div>
        <?php
          
          if (isset($_GET['area'])){
            $area = $_GET['area'];
          }else{
            $area = 'index';
          }
          
          if (isset($_GET['acao'])){
            $acao = $_GET['acao'];
          }else{
            $acao = 'index';
          }
          
          if ($area == 'cliente'){
            include './controller/ClienteController.class.php';
          }
          if ($area == 'cd'){
            include './controller/CdController.class.php';
          }
            
        ?>
    </body>
</html>
